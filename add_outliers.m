function [D] = add_outliers(D, num_outliers, factor)
    % function that is used for adding outliers to the dissimilarity matrix.
    % when factor is supplied the outliers are distances that are mutiplied by the factor.
    % otherwise - outliers are sampled randomly from D.

    N = size(D, 1);
    all_edges = zeros(N*(N-1)*0.5,2);
    edge_i = 1;
    for i = 1:N
        for j = i+1:N
            all_edges(edge_i,:) = [i j];
            edge_i = edge_i + 1;
        end
    end
            
    edges = all_edges(randsample(1:size(all_edges, 1), num_outliers), :);
    nnzD = D(D~=0);

    for edge_i = 1:size(edges,1)
       edge = edges(edge_i,:);

       if isequal(edge(1), edge(2)) == 0
           % D(edge(1),edge(2)) = D(edge(1),edge(2)) * 10;
           if nargin < 3
               D(edge(1),edge(2)) = randsample(nnzD(:), 1);
           else
               D(edge(1),edge(2)) = D(edge(1),edge(2)) * factor;
           end
           
           D(edge(2),edge(1)) = D(edge(1),edge(2)); 
       end
    end
end