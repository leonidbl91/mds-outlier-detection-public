function shepard_diagram(D, X, label, FilterM)
%{
Draw shepard diagram of the correct outliers.
Also should present the outlier edges in a different color.
%}

DX = squareform(pdist(X));

if nargin < 4
    plot(D(:),DX(:), 'xb');
    % legend(label);
else
    D_fil = D(FilterM ~= 0);
    DX_fil = DX(FilterM ~= 0);
    D_rem = D(FilterM == 0);
    DX_rem = DX(FilterM == 0);
    plot(D_fil(:),DX_fil(:), 'xb', D_rem(:),DX_rem(:), 'or');
end



xlabel('D_{nm}');
ylabel('d_{nm}(X)');


end