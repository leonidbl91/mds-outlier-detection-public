function [X2] = my_align(X1,X2)
% returns X1 and X2 that are rigidly aligned.
% X1 is unchanged
n = size(X1,1);
[R,t] = rigid_transform_3D(X2,X1);
X2_new = (R*X2') + repmat(t, 1, n);
X2 = X2_new';

end