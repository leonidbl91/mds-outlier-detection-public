function [D_out, average_factor] = add_gaussian_outliers(D, sigma) 
% Method adding log-normal gaussian noise to the dissimilarity matrix D.

N = size(D,1);
LogFactors = sigma*squareform(randn(1,N*(N-1)/2));
Factors = (2 .^ LogFactors);
D_out = D .* Factors;
average_factor = mean(Factors(:));
end