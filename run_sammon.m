function run_sammon()
% run an example that shows sammon MDS results 
% ones on shortened edges and on enlarged ones.

N = 30;
% X = normrnd(0,1,[N,2]);
min = [-10 -10]; 
max = [10 10]; 
X = bsxfun(@plus, min, bsxfun(@times, max-min,rand(N,2)));

D_gt = squareform(pdist(X));
D_shorter = add_outliers(D_gt, 3, 0.03);
D_longer = add_outliers(D_gt, 3, 4);

X_mds = mdscale(D_shorter, 2);
X_mds = my_align(X,X_mds);
X_lmds = mdscale(D_longer, 2);
X_lmds = my_align(X,X_lmds);

W = 1 ./ D_shorter .* (ones(N,N) - eye(N));
Wl = 1 ./ D_longer .* (ones(N,N) - eye(N));
X_smds = mdscale(D_shorter, 2, 'Weights', W, 'Criterion', 'metricstress', 'Start', 'random');
X_smds = my_align(X,X_smds);
X_lsmds = mdscale(D_longer, 2, 'Weights', Wl, 'Criterion', 'metricstress', 'Start', 'random');
X_lsmds = my_align(X,X_lsmds);


plot_sammon(X, D_gt, D_shorter, X_mds, X_smds, '-r', '-b');

plot_sammon(X, D_gt, D_longer, X_lmds, X_lsmds, '-r', '-b');

end

function  plot_sammon(X, D_gt, D, X_mds, X_smds, shortLine, longLine)
N = size(X,1);
% plotting results
figure
% subplot(1,3,1);
scatter(X(:,1), X(:,2), 'bo', 'LineWidth', 1.5);
[outliers_r, outliers_c] = find(D_gt ~= D);
for ij = [outliers_r outliers_c]'
    if D(ij(1), ij(2)) < D_gt(ij(1),ij(2))
        edge_plot(X(ij(1),:), X(ij(2),:), shortLine);
    else
        edge_plot(X(ij(1),:), X(ij(2),:), longLine);
    end
end


%subplot(1,3,2)
figure
for i = 1:N
    edge_plot(X(i,:), X_mds(i,:));
end
% title('Classic MDS');

% subplot(1,3,3)
figure
for i = 1:N
    edge_plot(X(i,:), X_smds(i,:));
end

end