
function edge_plot(vec1, vec2, lineParams)
   hold on;
   if nargin == 2
       lineParams='-k';
       scatter(vec1(1), vec1(2), 'bo', 'LineWidth', 1.5);
       scatter(vec2(1), vec2(2), 'rx', 'LineWidth',2);
   end

   plot([vec1(1), vec2(1)], [vec1(2), vec2(2)], lineParams, 'LineWidth', 1);
   hold off;
end