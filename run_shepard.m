function run_shepard()
% runs an example shepard diagram with our method.

N = 100;
% X = normrnd(0,1,[N,2]);
min = [-100 -100]; 
max = [100 100]; 
X_gt = bsxfun(@plus, min, bsxfun(@times, max-min,rand(N,2)));
num_outliers = 100;
D_gt = squareform(pdist(X_gt));
D_out = add_outliers(D_gt, num_outliers);


filtM = triangle_filter(D_out, 0);
[X, stress] = triangleMDS(D_out, 2, 0);

figure
shepard_diagram(D_out, X, 'Triangle Filter MDS',filtM);

end